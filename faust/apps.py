from django.apps import AppConfig


class FaustConfig(AppConfig):
    name = 'faust'
