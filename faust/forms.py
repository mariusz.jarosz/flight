from django import forms
from django_countries.fields import CountryField
from .models import Tourist, Flight



class TouristForm(forms.ModelForm):
    class Meta:
        model = Tourist
        fields = ('firstname', 'lastname', 'sex',
                 'country', 'notes', 'date_of_birth', 'flights')

    def __init__ (self, *args, **kwargs):
        super(TouristForm, self).__init__(*args, **kwargs)
        self.fields["flights"].widget = forms.widgets.CheckboxSelectMultiple()
        self.fields["flights"].queryset = Flight.objects.all()



class FlightForm(forms.ModelForm):

    class Meta:
        model = Flight
        fields = ('name', 'departure', 'arrival',
                 'seats', 'price', 'tourists')

        
    def __init__ (self, *args, **kwargs):
        super(FlightForm, self).__init__(*args, **kwargs)
        self.fields["tourists"].widget = forms.widgets.CheckboxSelectMultiple()
        self.fields["tourists"].queryset = Tourist.objects.all()


