from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField

# Create your models here.

class Tourist(models.Model):
    SEX_CHOICES = (
    ('F', 'Female',),
    ('M', 'Male',),
    ('U', 'Unsure',),
    )
    
    firstname   = models.CharField(max_length=120)
    lastname    = models.CharField(max_length=120)
    sex = models.CharField(
        max_length=1,
        choices=SEX_CHOICES,
    )
    
    country     = CountryField(blank_label='(select country)')
    notes       = models.TextField(blank=True, null=True)
    date_of_birth = models.DateTimeField()
    flights = models.ManyToManyField("Flight", blank=True, null=True)
    
    def __str__(self):
        return f"{self.firstname}"

class Flight(models.Model):
    name = models.CharField(max_length=120, default="test")
    departure = models.DateTimeField()
    arrival = models.DateTimeField()
    seats = models.PositiveIntegerField()
    price = models.DecimalField(decimal_places=2, max_digits=1000)
    tourists = models.ManyToManyField(Tourist, blank=True, null=True)
    
    def __str__(self):
        return self.name

 

