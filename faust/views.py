from django.shortcuts import render, get_object_or_404, redirect
from .forms import TouristForm, FlightForm
from .models import Tourist, Flight

# Create your views here.
def index_site(request):
    
    return render(request, 'faust/index.html', {})


def tourist_list(request):
    tourists = Tourist.objects.all().order_by('lastname')
    
    return render(request, 'faust/tourist_list.html', {'tourists':tourists})

def tourist_detail(request, pk):
    t_detail = get_object_or_404(Tourist, pk=pk)
    return render(request, 'faust/tourist_detail.html', {'t_detail': t_detail})

def tourist_new(request):
    if request.method == "POST":
        form = TouristForm(request.POST)
        if form.is_valid():
            tourist = form.save(commit=False)
            tourist.save()
            form.save_m2m()
            return redirect('tourist_detail', pk=tourist.pk)
    else:
        form = TouristForm()
    return render(request, 'faust/tourist_edit.html', {'form': form})



def tourist_edit(request, pk):
    tourist = get_object_or_404(Tourist, pk=pk)
    if request.method == "POST":
        form = TouristForm(request.POST, instance=tourist)
        if form.is_valid():
            tourist = form.save(commit=False)
            tourist.save()
            form.save_m2m()
            return redirect('tourist_detail', pk=tourist.pk)
    else:
        form = TouristForm(instance=tourist)
    return render(request, 'faust/tourist_edit.html', {'form': form})

def tourist_remove(request, pk):
    tourist = get_object_or_404(Tourist, pk=pk)
    tourist.delete()
    return redirect('tourist_list')



def flight_list(request):
    flights = Flight.objects.all().order_by('name') 
    return render(request, 'faust/flight_list.html', {'flights':flights})

def flight_detail(request, pk):
    f_detail = get_object_or_404(Flight, pk=pk)
    return render(request, 'faust/flight_detail.html', {'f_detail': f_detail})

def flight_new(request):
    if request.method == "POST":
        form = FlightForm(request.POST)
        if form.is_valid():
            flight = form.save(commit=False)
            flight.save()
            form.save_m2m()
            return redirect('flight_detail', pk=flight.pk)
    else:
        form = FlightForm()
    return render(request, 'faust/flight_edit.html', {'form': form})

def flight_edit(request, pk):
    flight = get_object_or_404(Flight, pk=pk)
    if request.method == "POST":
        form = FlightForm(request.POST, instance=flight)
        if form.is_valid():
            flight = form.save(commit=False)
            flight.save()
            form.save_m2m()
            return redirect('flight_detail', pk=flight.pk)
    else:
        form = FlightForm(instance=flight)
    return render(request, 'faust/flight_edit.html', {'form': form})

def flight_remove(request, pk):
    flight = get_object_or_404(Flight, pk=pk)
    flight.delete()
    return redirect('flight_list')