from django.contrib import admin

# Register your models here.
from .models import Tourist, Flight

admin.site.register(Tourist)
admin.site.register(Flight)