from django.urls import path
from . import views


urlpatterns = [
    path('', views.index_site, name='index_site'),

    path('tourist/', views.tourist_list, name='tourist_list'),
    path('tourist/<int:pk>', views.tourist_detail, name='tourist_detail'),
    path('tourist/new/', views.tourist_new, name='tourist_new'),
    path('tourist/<int:pk>/edit/', views.tourist_edit, name='tourist_edit'),
    path('tourist/<int:pk>/remove/', views.tourist_remove, name='tourist_remove'),

    path('flight/', views.flight_list, name='flight_list'),
    path('flight/<int:pk>', views.flight_detail, name='flight_detail'),
    path('flight/new/', views.flight_new, name='flight_new'),
    path('flight/<int:pk>/edit/', views.flight_edit, name='flight_edit'),
    path('flight/<int:pk>/remove/', views.flight_remove, name='flight_remove'),


]